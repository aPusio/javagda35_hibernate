package org.example.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.example.HibernateFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class EntityDao<T> {
	public void save(T entity){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(entity);
		transaction.commit();
		session.close();
	}

	public T getById(Class<T> classType, Long id){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		T entity = session.get(classType, id);
		session.close();
		return entity;
	}

	public void delete(Class<T> classType, Long id){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		T entity = session.get(classType, id);
		session.delete(entity);
		transaction.commit();
		session.close();
	}

	public void update(T entity){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.update(entity);
		transaction.commit();
		session.close();
	}

	public List<T> getAllGeneric(Class<T> classType){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<T> query = criteriaBuilder.createQuery(classType);
		Root<T> from = query.from(classType);
		query.select(from);

		List<T> resultList = session.createQuery(query).getResultList();
		session.close();
		return resultList;
	}
}
