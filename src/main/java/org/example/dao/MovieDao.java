package org.example.dao;

import java.util.List;

import org.example.HibernateFactory;
import org.example.model.Movie;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class MovieDao extends EntityDao<Movie>{
	public List<Movie> getAll(){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<Movie> movies = session.createQuery("From Movie", Movie.class).list();
		session.close();
		return movies;
	}

	public List<String> getAllMovieTitles(){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<String> movieTitles = session.createQuery("SELECT M.title FROM Movie M", String.class).list();
		session.close();
		return movieTitles;
	}

	public Movie getByIdWithBadges(Long id){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Movie movie = session.get(Movie.class, id);
		Hibernate.initialize(movie.getBadges());
		session.close();
		return movie;
	}
}
