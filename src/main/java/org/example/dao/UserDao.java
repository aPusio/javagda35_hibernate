package org.example.dao;

import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.example.HibernateFactory;
import org.example.model.Movie;
import org.example.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class UserDao extends EntityDao<User> {
	public List<User> getAll() {
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<User> users = session.createQuery("From User", User.class).list();
		session.close();
		return users;
	}

	public void nPlusOneExample() {
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<User> users = session.createQuery("From User u JOIN fetch u.movies", User.class).list();
		for (User user : users) {
			Set<Movie> movies = user.getMovies();
			for (Movie movie : movies) {
				System.out.println(movie.getTitle());
			}
		}
		session.close();
	}

	public List<User> usersWithAInLogin(){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<User> query = builder.createQuery(User.class);
		Root<User> root = query.from(User.class);
		query.select(root)
			.where(builder.like(root.get("login"), "%a%"));

		List<User> resultList = session.createQuery(query).getResultList();
		session.close();
		return resultList;
	}

	public Long countUserIds(){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		Root<User> root = query.from(User.class);
		query.select(builder.count(root.get("id")))
			.where(builder.like(root.get("login"), "%f%"));

		Long singleResult = session.createQuery(query).getSingleResult();
		session.close();
		return singleResult;
	}

	public Long countUserIdsContainsfAnda(){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		Root<User> root = query.from(User.class);
		Predicate loginContainsF = builder.like(root.get("login"), "%f%");
		Predicate loginContainsA = builder.like(root.get("login"), "%a%");
		query.select(builder.count(root.get("id")))
			.where(builder.and(loginContainsA, loginContainsF));

		Long singleResult = session.createQuery(query).getSingleResult();
		session.close();
		return singleResult;
	}
}
