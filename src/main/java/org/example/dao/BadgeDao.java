package org.example.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.example.HibernateFactory;
import org.example.model.Badge;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class BadgeDao extends EntityDao<Badge>{
	public List<Badge> getAll(){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<Badge> badges = session.createQuery("From Badge", Badge.class).list();
		session.close();
		return badges;
	}

	public List<Badge> getIdLessThanTwo(){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Badge> query = builder.createQuery(Badge.class);
		Root<Badge> root = query.from(Badge.class);
		query.select(root).where(builder.lt(root.get("id"), 2));

		List<Badge> resultList = session.createQuery(query).getResultList();
		session.close();
		return resultList;
	}
}
