package org.example.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.example.model.Author;
import org.example.HibernateFactory;
import org.example.model.PartialAuthorData;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

public class AuthorDao extends EntityDao<Author> {
	public List<Author> getAll() {
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<Author> authors = session.createQuery("From Author", Author.class).list();
		session.close();
		return authors;
	}

	public List<Author> getAllUsingCriteriaQuerry(){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Author> query = builder.createQuery(Author.class);
		Root<Author> scope = query.from(Author.class);
		query.select(scope);

		List<Author> resultList = session.createQuery(query).getResultList();
		session.close();
		return resultList;
	}

	public List<Author> getByNameAndSurname(String methodName, String methodSurname) {
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
//		List<Author> resultList = session.createQuery(
//			"From Author a where a.name ='" + name +
//				"' and a.surname = '" + surname + "'",
//			Author.class).getResultList();

		Query<Author> query = session.createQuery(
			"From Author a where a.name = :filtername and a.surname = :filtersurname",
			Author.class);
		query.setParameter("filtername", methodName);
		query.setParameter("filtersurname", methodSurname);
		List<Author> resultList = query.getResultList();
		session.close();
		return resultList;
	}

	public void getAllAuthorNamesAndSurnames(){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<Object[]> resultList = session.createQuery("SELECT a.name, a.surname FROM Author a", Object[].class)
										.getResultList();
		for (Object[] result: resultList) {
			System.out.println(" ------------------- ");
			System.out.println("name: " + result[0]);
			System.out.println("surname: " + result[1]);
		}

		session.close();
	}

	public List<PartialAuthorData> getPartialAuthorData(){
		SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Query<PartialAuthorData> query = session.createQuery(
			"SELECT NEW org.example.model.PartialAuthorData(a.name, a.surname) FROM Author a",
			PartialAuthorData.class);
		return query.getResultList();
	}
}
