package org.example;

import java.util.Collections;
import java.util.HashSet;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.example.dao.AuthorDao;
import org.example.dao.BadgeDao;
import org.example.dao.MovieDao;
import org.example.dao.UserDao;
import org.example.model.Address;
import org.example.model.Author;
import org.example.model.Badge;
import org.example.model.Movie;
import org.example.model.User;
import org.hibernate.validator.internal.engine.ConstraintViolationImpl;

public class App {
	public static void main(String[] args) {
		Author author = new Author(
			"a",
			"P",
			new Address("Gdansk", "Gdanska", "80-200"));
		Author author2 = new Author(
			"Noname",
			"Kowalski",
			new Address("Gdynia", "Gdynska", "80-100")
		);

		validateAuthor(author);

		Badge badge = new Badge("SUPER");
		Badge badge1 = new Badge("SLABY");
		HashSet<Badge> badges = new HashSet<>();
		badges.add(badge);
		badges.add(badge1);

		Movie movie = new Movie("Smierc w Wenecji");
		author.setMovie(movie);
		movie.setAuthor(author);
		movie.setBadges(badges);

		Movie movie2 = new Movie("Smierc w Wenecji");
		author2.setMovie(movie2);
		movie2.setAuthor(author2);
		movie2.setBadges(Collections.singleton(badge1));

		User user = new User("apusiof", "admin123");
		user.add(movie);
		movie.setUser(user);
		user.add(movie2);
		movie2.setUser(user);

//		AuthorDao authorDao = new AuthorDao();
//		authorDao.save(author);
//		authorDao.delete(2L);
//		Author authorById = authorDao.getById(2L);
//		author.setSurname("Kowalski");
//		authorDao.update(author);

		AuthorDao authorDao = new AuthorDao();
		MovieDao movieDao = new MovieDao();
		UserDao userDao = new UserDao();
		BadgeDao badgeDao = new BadgeDao();

		badgeDao.save(badge1);
		badgeDao.save(badge);

		userDao.save(user);
//		authorDao.save(author);
//		authorDao.save(author2);
			try{
				movieDao.save(movie);
			}catch (ConstraintViolationException cve){
				System.out.println(cve.getMessage());
			}
		movieDao.save(movie2);

//		authorDao.getAll().forEach(System.out::println);
//		authorDao.getByNameAndSurname("Adam", "P")
//			.forEach(System.out::println);
//		movieDao.getAllMovieTitles().forEach(System.out::println);

//		Movie movieFromDb = movieDao.getByIdWithBadges(1L);
//		Set<Badge> badgesFromMovie = movieFromDb.getBadges();
//		for (Badge singleBadge : badgesFromMovie) {
//			System.out.println(singleBadge.getName());
//		}

//		userDao.nPlusOneExample();

//		authorDao.getAllAuthorNamesAndSurnames();
//		authorDao.getPartialAuthorData()
//			.forEach(System.out::println);

//		authorDao.getAllUsingCriteriaQuerry().forEach(System.out::println);
//		authorDao.getAllGeneric(Author.class).forEach(System.out::println);
//		badgeDao.getIdLessThanTwo().forEach(System.out::println);

		System.out.println(userDao.countUserIds());

		HibernateFactory.getSessionFactory().close();
	}

	private static void validateAuthor(@Valid Author author) {

	}
}
