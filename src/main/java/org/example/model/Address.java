package org.example.model;

import javax.persistence.Embeddable;

import org.hibernate.validator.constraints.pl.PESEL;
import org.hibernate.validator.constraints.pl.REGON;

@Embeddable
public class Address {
	private String city;
	private String street;
	private String postCode;

	public Address() {
	}

	public Address(String city, String street, String postCode) {
		this.city = city;
		this.street = street;
		this.postCode = postCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	@Override
	public String toString() {
		return "Address{" +
				   "city='" + city + '\'' +
				   ", street='" + street + '\'' +
				   ", postCode='" + postCode + '\'' +
				   '}';
	}
}
