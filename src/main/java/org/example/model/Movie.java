package org.example.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MOVIE_TABLE")
public class Movie {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String title;
	@OneToOne(cascade = CascadeType.ALL)
	private Author author;

	@ManyToOne
	private User user;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "MOVIE_BADGE_FK",
		joinColumns = {@JoinColumn(name = "MOVIE_FK")},
		inverseJoinColumns = {@JoinColumn(name = "BADGE_FK")})
	private Set<Badge> badges = new HashSet<>();

	public Movie() {
	}

	public Movie(String title) {
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Badge> getBadges() {
		return badges;
	}

	public void setBadges(Set<Badge> badges) {
		this.badges = badges;
	}

	@Override
	public String toString() {
		return "Movie{" +
				   "id=" + id +
				   ", title='" + title + '\'' +
				   '}';
	}
}
